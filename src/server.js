const express     = require('express');
const bodyParser  = require('body-parser');
const cors        = require('cors');
const router      = require('./router');

const app        = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const startExpress = async (port, baseUrl) => {
  try {
    app.use(baseUrl, router);
    await app.listen(port)
      .on('error', err => {
        throw new Error(`Could not start Express server on port ${port}`)
      });
    console.log('Express server started, listening in on ', port);
    return;
  } catch (error) {
    throw error
  }
}

module.exports = {
  app,
  startExpress
};