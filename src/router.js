const express           = require('express')
const router            = express.Router()
const { name, version } = require('./../package.json')
const { notFound }      = require('./utils')
const articles          = require('./articles')

router.get('/version', (_, res) => res.json({ name, version }))
router.use('/'       , articles)
router.all('*'       , notFound)

module.exports = router