const mongoose = require('mongoose')

const articleSchema = new mongoose.Schema({
  author: {
    type: String,
    required: true
  },
  authorId: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  subTitle: {
    type: String,
    required: true
  },
  lead: String,
  abstract: String,
  category: String,
  keywords: [String],
  content: String,
  stats: Object,
  active: {
    type: Boolean,
    default: true
  },
  published_at: {
    type: Date,
    default: null
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  }
})

module.exports = mongoose.model('Article', articleSchema)
