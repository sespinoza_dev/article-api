const jwt        = require('jsonwebtoken')
const User       = require('../users/model')
const Artices    = require('../articles/model')

const guard = async (req, res, next) => {
  try {
    const authorization = req.header('Authorization')
    if (!authorization)
      throw new Error('unauthorized, no token provided')

    const token = authorization.replace('Bearer ', '')
    const data  = jwt.verify(token, process.env.SECRET)
    const user  = await User.findOne({
      $and: [
        { _id: data._id },
        {'tokens.token': token }
      ]
    })

    if (!user)
      throw new Error('unauthorized, user not found')
    
    if(user.role != 'writer' && user.role != 'admin')
      throw new Error(`forbiden, user rol: ${user.role}`)
    
    req.user = user
    req.token = token
    next()

  } catch (error) {
    switch (true) {
      case /unauthorized/.test(error):
        console.log('[guard] unauthorized access', error)
        res
          .status(401)
          .json({ reason: 'unauthorized', message: 'authenticate first' })
          .end()
        break
      case /forbiden/.test(error):
        console.warn('[guard] forbiden request attempt, user', error)
        res
          .status(403)
          .json({ reason: 'forbiden', message: 'not allowed to use this resourse' })
          .end()
        break;
    
      default:
        console.error('[guard] INTERNAL ERROR', error)
        res
          .status(500)
          .json({ reason: 'server internal error', message: error.message })
          .end()
        break
    }
  }
}

const ownerAuthorization = async (req, res, next) => {
  try {
    if (req.user.role === 'admin')
      next()

    const article  = await Artices.findOne({ _id: req.params.id })
    
    if (!article)
      throw new Error('not found')
    
    if (article.authorId != req.user._id)
      throw new Error('forbiden')
    
    next()

  } catch (error) {
    switch (error.message) {
      case 'not found':
        console.log('[ownerAuthorization] not found')
        res
          .status(404)
          .json({ reason: 'not found', message: 'could not find the resource you are looking for' })
          .end()
        break
      case 'forbiden':
        console.log('[ownerAuthorization] forbiden')
        res
          .status(403)
          .json({ reason: 'forbiden', message: 'You do not have privileges on this resource' })
          .end()
        break;
    
      default:
        console.error('[ownerAuthorization] INTERNAL ERROR', error)
        res
          .status(500)
          .json({ reason: 'server internal error', message: error.message })
          .end()
        break
    }
  }
}

module.exports = {
  guard,
  ownerAuthorization,
}