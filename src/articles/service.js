const Article     = require('./model')
const R           = require('ramda')
const readingTime = require('reading-time')

exports.getArticles = async () => {
  try {
    const articles = await Article.find({
      active: true,
      "published_at": { $ne: null } },
      { content: 0 })
      .sort( { published_at: -1 } )
    return articles
  } catch (error) {
    throw error
  }
}

exports.getAuthorArticles = async authorId => {
  try {
    const articles = await Article.find({ authorId }, { content: 0 })
    return articles
  } catch (error) {
    throw error
  }
}

exports.getSingleArticle = async id => {
  try {
    const article = await Article.findById(id)
    return article
  } catch (error) {
    throw error
  }
}

exports.createArticle = async (user, payload) => {
  try {
    console.log('about to create in db...')
    console.log(payload)
    const withStatsAndAuthor = R.pipe(
      R.assoc('stats', readingTime(payload.content)),
      R.assoc('authorId', user._id)
    )(payload)
    const article = new Article(withStatsAndAuthor)
    return article.save()
  } catch (error) {
    throw error
  }
}

exports.publishArticle = async ({ id }) => {
  try {
    const update = await Article.findByIdAndUpdate(id, { published_at: Date.now() }, { new: true })
    return update
  } catch (error) {
    throw error
  }
}

exports.updateArticle = async ({ id, payload }) => {
  try {
    const withStats = R.assoc('stats', readingTime(payload.content), payload)
    const { ...updateData } = withStats
    const update = await Article.findByIdAndUpdate(id, updateData, { new: true })
    return update
  } catch (error) {
    throw error
  }
}

exports.deleteArticle = async id => {
  try {
    const article = await Article.findByIdAndRemove(id)
    return article
  } catch (err) {
    throw error
  }
}
