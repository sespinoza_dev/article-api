require('console-stamp')(console, '[HH:MM:ss.l]')
const mongoose       = require('mongoose')
const { startExpress }  = require('./src/server.js')
const { name, version } = require('./package.json')

if (!process.env.SECRET) {
  console.log('some environment variables missing')
  process.exit(-1)
}

const MONGO_URL = process.env.MONGO_URL || 'mongodb://localhost/portfolio'
const PORT = process.env.PORT || 4003
const BASE_URL = process.env.BASE_URL || '/articles/api/v1'

console.log(`Service: ${name}, version: ${version}`)

const start = async () => {
  try {
    const options = {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true
    }
    await mongoose.connect(MONGO_URL, options)
    console.log('MongoDB connected.')

    await startExpress(PORT, BASE_URL)
    console.log('Server started.')

  } catch (error) {
    console.error(error)
    process.exit(1)
  }
}

start()