# Articles API

Api for retrieving articles from server.

## Notes

The goal here is to learn how to make a well design
and secure API with Nodejs, Express and Mongodb.

So that way the API must:

## Todo:

- [x] CRUD articles.
- [ ] pagination on articles.
- [x] authotization.
- [ ] documentation with swager.

## Installation

```bash
yarn
```

There's has to be a .env file and a data directory.

## Usage

Start the application with:

```bash
yarn start
```

Create a `.env` file in the root of the repository with the following content.

```bash
export PORT=4004,
export BASE_URL="/articles/api/v1",
export SECRET="",
```

and export it to your terminal session with

```bash
. .env
```

If you want to run it in production I would suggest that you build the image and use a `docker-compose.yml` to load the environmental variables.

## API

The vertsion api:

| method | endpoint | response                                           |
|--------|----------|----------------------------------------------------|
| GET    | `/api`   | {  "service": `<string>`,  "version": `<string>` } |

Article api:

Root url: `/articles/api/v1`.

| method | endpoint | description               |
|--------|----------|---------------------------|
| GET    | `/`      | list all aticles          |
| GET    | `/:id`   | get single article by id  |
| POST   | `/`      | creates and aticle        |
| PUT    | `/:id`   | updates an article by id  |
| DELETE | `/:id`   | deletes an article by id  |

todo: crud articles

## Contributing

todo.

## test it

get all

```console
curl -s localhost:3000/articles/api/v1 | jq .
```

create

```console
curl -s -XPOST -H "content-type: application/json" -H "authorization: $token" -d '{ "author": "S.Espinoza", "title": "My first Article", "subTitle": "an overview of my world", "lead": "...hello world", "abstract":"All you need to know about this blog site", "published": null, "category": "general", "keywords": ["general", "intro"] ,"content": "This is only the begining" }' localhost:3000/articles/api/v1/ | jq .
```

get

```console
curl -s localhost:3000/articles/api/v1/5d3663b08614ba292aa587d3 | jq .
```

delete

```console
 curl -s -XDELETE -H "content-type: application/json"  -H "authorization: token" localhost:3000/articles/api/v1/5d365df9e98eff234f7d356f | jq .
```

update

```console
curl -s -XPUT -H "content-type: application/json" -H "authorization: token" -d '{ "author": "S.Espinoza", "title": "My first updated Article", "subTitle": "an overview of my world", "lead": "...hello world", "abstract":"All you need to know about this blog site", "published": null, "category": "general", "keywords": ["general", "intro", "Test"] ,"content": "This is only the begining" }' localhost:3000/articles/api/v1/5d366354b69a3526348f638a
```

## Contributors

- [[sespinoz]](https://gitlab.com/sespinoz) Samuel Espinoza - creator, maintainer
