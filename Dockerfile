FROM node:10.15.3-alpine

LABEL author="S.Espinoza <espinoza.jimenez@gmail.com>"

# Dependencies
RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh && \
    apk add curl curl-dev git && \
    apk add python python-dev gcc libffi-dev g++ make

# Directory
RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app/
COPY index.js package.json package-lock.json /usr/src/app/
COPY src /usr/src/app/src

# Application
RUN npm install --production
EXPOSE 4003

CMD ["node", "/usr/src/app/index.js"]
